namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDALMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.C__MigrationHistory",
                c => new
                    {
                        MigrationId = c.String(nullable: false, maxLength: 128),
                        ContextKey = c.String(),
                        Model = c.Binary(),
                        ProductVersion = c.String(),
                    })
                .PrimaryKey(t => t.MigrationId);
            
            CreateTable(
                "dbo.Hostels",
                c => new
                    {
                        HostelID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FreePlaces = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HostelID);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        RoomID = c.Int(nullable: false, identity: true),
                        Size = c.Int(nullable: false),
                        Index = c.Int(nullable: false),
                        Sex = c.Int(nullable: false),
                        FreePlaces = c.Int(nullable: false),
                        Hostel_HostelID = c.Int(),
                    })
                .PrimaryKey(t => t.RoomID)
                .ForeignKey("dbo.Hostels", t => t.Hostel_HostelID)
                .Index(t => t.Hostel_HostelID);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Email = c.String(),
                        Sex = c.Int(nullable: false),
                        Room_RoomID = c.Int(),
                    })
                .PrimaryKey(t => t.StudentID)
                .ForeignKey("dbo.Rooms", t => t.Room_RoomID)
                .Index(t => t.Room_RoomID);
            
            CreateTable(
                "dbo.Questionnaires",
                c => new
                    {
                        QuestionnaireID = c.Int(nullable: false, identity: true),
                        Activities = c.String(),
                        Interests = c.String(),
                        FavoriteMusic = c.String(),
                        FavoriteMovies = c.String(),
                        FavoriteTvShows = c.String(),
                        FavoriteBooks = c.String(),
                        Smoking = c.String(),
                        Drinking = c.String(),
                        Student_StudentID = c.Int(),
                    })
                .PrimaryKey(t => t.QuestionnaireID)
                .ForeignKey("dbo.Students", t => t.Student_StudentID)
                .Index(t => t.Student_StudentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "Hostel_HostelID", "dbo.Hostels");
            DropForeignKey("dbo.Students", "Room_RoomID", "dbo.Rooms");
            DropForeignKey("dbo.Questionnaires", "Student_StudentID", "dbo.Students");
            DropIndex("dbo.Questionnaires", new[] { "Student_StudentID" });
            DropIndex("dbo.Students", new[] { "Room_RoomID" });
            DropIndex("dbo.Rooms", new[] { "Hostel_HostelID" });
            DropTable("dbo.Questionnaires");
            DropTable("dbo.Students");
            DropTable("dbo.Rooms");
            DropTable("dbo.Hostels");
            DropTable("dbo.C__MigrationHistory");
        }
    }
}
