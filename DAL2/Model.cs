namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=ModelConnection")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Hostel> Hostels { get; set; }
        public virtual DbSet<Questionnaire> Questionnaires { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Student> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hostel>()
                .HasMany(e => e.Rooms)
                .WithOptional(e => e.Hostel)
                .HasForeignKey(e => e.Hostel_HostelID)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.Students)
                .WithOptional(e => e.Rooms)
                .HasForeignKey(e => e.Room_RoomID);

            modelBuilder.Entity<Student>()
                .HasMany(e => e.Questionnaires)
                .WithOptional(e => e.Students)
                .HasForeignKey(e => e.Student_StudentID);
        }
    }
}
