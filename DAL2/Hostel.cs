namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Hostel
    {
        public Hostel()
        {
            Rooms = new HashSet<Room>();
        }

        [Key]
        public int HostelID { get; set; }

        public string Title { get; set; }

        public int FreePlaces { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
    }
}
