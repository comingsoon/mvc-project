namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Questionnaire
    {
        [Key]
        public int QuestionnaireID { get; set; }

        public string Activities { get; set; }

        public string Interests { get; set; }

        public string FavoriteMusic { get; set; }

        public string FavoriteMovies { get; set; }

        public string FavoriteTvShows { get; set; }

        public string FavoriteBooks { get; set; }

        public string Smoking { get; set; }

        public string Drinking { get; set; }

        public int? Student_StudentID { get; set; }

        public virtual Student Students { get; set; }
    }
}
