namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Student
    {
        public Student()
        {
            Questionnaires = new HashSet<Questionnaire>();
        }

        [Key]
        public int StudentID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public int Sex { get; set; }

        public int? Room_RoomID { get; set; }

        public virtual ICollection<Questionnaire> Questionnaires { get; set; }

        public virtual Room Rooms { get; set; }
    }
}
