namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Room
    {
        public Room()
        {
            Students = new HashSet<Student>();
        }

        [Key]
        public int RoomID { get; set; }

        public int Size { get; set; }

        public int Index { get; set; }

        public int Sex { get; set; }

        public int FreePlaces { get; set; }

        public int? Hostel_HostelID { get; set; }

        public virtual Hostel Hostel { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}
