namespace Stud.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionnaire : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Questionnaires",
                c => new
                    {
                        QuestionnaireID = c.Int(nullable: false, identity: true),
                        Activities = c.String(),
                        Interests = c.String(),
                        FavoriteMusic = c.String(),
                        FavoriteMovies = c.String(),
                        FavoriteTvShows = c.String(),
                        FavoriteBooks = c.String(),
                        Smoking = c.String(),
                        Drinking = c.String(),
                        Student_StudentID = c.Int(),
                    })
                .PrimaryKey(t => t.QuestionnaireID)
                .ForeignKey("dbo.Students", t => t.Student_StudentID)
                .Index(t => t.Student_StudentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questionnaires", "Student_StudentID", "dbo.Students");
            DropIndex("dbo.Questionnaires", new[] { "Student_StudentID" });
            DropTable("dbo.Questionnaires");
        }
    }
}
