﻿using Stud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stud.Abstract
{
    interface IRoomsRepository
    {
        IEnumerable<DAL.Room> Rooms { get; }
    }
}
