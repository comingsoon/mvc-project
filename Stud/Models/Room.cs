﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Stud.Models
{
    public class Room
    {
        public int RoomID { get; set; }

        [Required()]
        public int Size { get; set; }

        public int FreePlaces { get; set; }

        [Required()]
        [Display(Name = "Room Index")]
        public int Index { get; set; }

        [EnumDataType(typeof(SexType))]
        [Required()]
        public SexType Sex { get; set; }

        public virtual List<Student> Students { get; set; }
        public virtual Hostel Hostel { get; set; }
    }
}