﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Stud.Models
{
    public class Questionnaire
    {
        public int QuestionnaireID { get; set; }

        public virtual Student Student { get; set; }

        [DataType(DataType.MultilineText)]
        public string Activities { get; set; }

        [DataType(DataType.MultilineText)]
        public string Interests { get; set; }

        [DataType(DataType.MultilineText)]
        public string FavoriteMusic { get; set; }

        [DataType(DataType.MultilineText)]
        public string FavoriteMovies { get; set; }

        [DataType(DataType.MultilineText)]
        public string FavoriteTvShows { get; set; }

        [DataType(DataType.MultilineText)]
        public string FavoriteBooks { get; set; }

        [DataType(DataType.MultilineText)]
        public string Smoking { get; set; }

        [DataType(DataType.MultilineText)]
        public string Drinking { get; set; }
    }
}