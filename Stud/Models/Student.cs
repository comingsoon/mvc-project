﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Stud.Models
{
    public enum SexType
    {
        Male = 0,
        Female = 1
    }

    public class Student
    {
        public int StudentID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public SexType Sex { get; set; }

        public virtual Room Room { get; set; }
    }
}