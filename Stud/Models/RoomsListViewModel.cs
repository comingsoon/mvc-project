﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stud.Models
{
    public class RoomsListViewModel
    {
        public IEnumerable<Room> Rooms { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}