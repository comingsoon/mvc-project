﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stud.Models
{
    public class Hostel
    {
        public int HostelID { get; set; }

        public string Title { get; set; }

        public int FreePlaces { get; set; }

        public virtual List<Room> Rooms { get; set; }
    }
}