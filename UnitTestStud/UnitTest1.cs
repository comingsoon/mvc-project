﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stud.Models;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Linq;
using Moq;
using Stud.Controllers;
using System.Collections.Generic;
using DAL;

using Stud.Models.HtmlHelpers;

namespace UnitTestStud
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CanPaginate()
        {
            // get this from database
            int pageIndex = 3;
            int roomsCount = 1;
            int roomIndex = 333;

            // Организация (arrange)
            
            RoomsController controller = new RoomsController();
            controller.pageSize = 3;

            // Действие (act)
            IEnumerable<Room> result = (IEnumerable<Room>)controller.Index(pageIndex).Model;

            // Утверждение (assert)
            List<Room> rooms = result.ToList();
            Assert.IsTrue(rooms.Count == roomsCount);
            Assert.AreEqual(rooms[0].Index, roomIndex);
        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {

            // Организация - определение вспомогательного метода HTML - это необходимо
            // для применения расширяющего метода
            HtmlHelper myHelper = null;

            // Организация - создание объекта PagingInfo
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // Организация - настройка делегата с помощью лямбда-выражения
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Действие
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            // Утверждение
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>"
                + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>"
                + @"<a class=""btn btn-default"" href=""Page3"">3</a>",
                result.ToString());
        }
    }
}
